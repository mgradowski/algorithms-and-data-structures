#pragma once
#include <inttypes.h>
#include <stdint.h>
#include <stdlib.h>
#include "tree.h"
#include "mast.h"
#include <limits.h>

#define NO_MATCH SIZE_MAX
#define PAYOFF(CTX, I, J) ( (CTX)->payoffs[(I)*((CTX)->items_count)+(J)] )


typedef struct {
    uint_fast32_t* payoffs;
    double*        prices;
    size_t*        matches;
    size_t*        unmatched;
    size_t         unmatched_count;
    size_t         buyers_count;
    size_t         buyers_max;
    size_t         items_count;
    size_t         items_max;
    double         eps;
} match_ctx_t;


extern match_ctx_t*
match_ctx_thd_get(void);

extern match_ctx_t*
match_ctx_alloc(void);

extern void
match_ctx_init(match_ctx_t*);

extern void
match_ctx_resize(match_ctx_t*, size_t, size_t);

extern void
match_ctx_thd_resize(size_t, size_t);

extern uint_fast32_t
match_ctx_exec(match_ctx_t*);
