#include <stdio.h>
#include "tree.h"
#include "mast.h"
#include "match.h"

int
main(void)
{   
    size_t   tree_buf_size;
    tree_t** tree_buf;
    size_t   i;
    size_t   j;

    mast_ctx_thd_init();

    scanf(" %zu", &tree_buf_size);
    tree_buf = (tree_t**)malloc(tree_buf_size*sizeof(tree_t*));

    for (i = 0; i < tree_buf_size; i++)
    {
        tree_buf[i] = tree_scan();
    }
    
    for (i = 0; i < tree_buf_size; i++)
    {
        for (j = i + 1; j < tree_buf_size; j++)
        {
            printf("%zu\n",
                tree_buf[i]->leaf_count - mast_thd(tree_buf[i], tree_buf[j])
                );
        }
    }

    for (i = 0; i < tree_buf_size; i++)
    {
        tree_free(tree_buf[i]);
    }

    free(tree_buf);

    return EXIT_SUCCESS;
}
