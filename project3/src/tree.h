#pragma once
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#define PRIval "zu"
#define SCNval "zu"

#define IS_LEAF(N) ( (N)->child  == NULL )
#define IS_ROOT(N) ( (N)->parent == NULL )

typedef size_t value_t;

typedef struct node_t {
    value_t        value;
    struct node_t* parent;
    struct node_t* child;
    struct node_t* sibling;  
} node_t;

typedef struct {
    node_t*  root;
    node_t** node_lookup;
    size_t   node_count;
    size_t   leaf_count;
} tree_t;

extern size_t
node_count_children(node_t const*);

extern void
tree_free(tree_t*);

extern tree_t*
tree_fscan(FILE* restrict);

extern tree_t*
tree_scan(void);

extern void
tree_fprint(FILE* restrict, tree_t const*);

extern void
tree_print(tree_t const*);
