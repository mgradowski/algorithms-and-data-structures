#pragma once
#include <limits.h>
#include <stdint.h>
#include <stdlib.h>
#include "compatibility.h"
#include "match.h"
#include "tree.h"

#define UNEVALUATED UINT_FAST32_MAX
#define MAST_CTX_LOOKUP(CTX, I, J) ( (CTX)->lookup[((I)*((CTX)->t2->node_count))+(J)] )

typedef struct {
    uint_fast32_t*         lookup;
    size_t                 lookup_maxsize;
    tree_t const* restrict t1;
    tree_t const* restrict t2;
    match_ctx_t*           match_ctx;
} mast_ctx_t;


extern mast_ctx_t*
mast_ctx_thd_get(void);

extern mast_ctx_t*
mast_ctx_alloc(void);

extern void
mast_ctx_init(mast_ctx_t*);

extern void
mast_ctx_thd_init(void);

extern void
mast_ctx_resize(mast_ctx_t*, size_t);

extern void
mast_ctx_thd_resize(size_t);

extern void
mast_ctx_setup(mast_ctx_t*, tree_t const* restrict, tree_t const* restrict);

extern void
mast_ctx_thd_setup(tree_t const* restrict, tree_t const* restrict);

extern uint_fast32_t
mast(mast_ctx_t*, tree_t const* restrict, tree_t const* restrict);

extern uint_fast32_t
mast_thd(tree_t const* restrict, tree_t const* restrict);
