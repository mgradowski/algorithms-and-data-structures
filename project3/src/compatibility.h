#pragma once

#ifndef thread_local
    #ifdef __GNUC__
        #define thread_local __thread
    #else
        #define thread_local __declspec(thread)
    #endif
#endif

#ifdef __cplusplus
    #define restrict
#endif
