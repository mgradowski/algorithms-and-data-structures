#include <inttypes.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdlib.h>
#include "compatibility.h"
#include "mast.h"
#include "match.h"


typedef struct {
    size_t fst;
    size_t snd;
} item_preference_t;


static thread_local match_ctx_t match_ctx_thd = {
    .payoffs         = NULL,
    .prices          = NULL,
    .matches         = NULL,
    .unmatched       = NULL,
    .unmatched_count = 0,
    .buyers_count    = 0,
    .buyers_max      = 0,
    .items_count     = 0,
    .items_max       = 0,
    .eps             = 0
};


extern match_ctx_t*
match_ctx_thd_get(void)
{
    return &match_ctx_thd;
}

extern match_ctx_t*
match_ctx_alloc(void)
{
    return (match_ctx_t*)malloc(sizeof(match_ctx_t));
}

extern void
match_ctx_init(match_ctx_t* ctx)
{
    ctx->payoffs         = NULL;
    ctx->prices          = NULL;
    ctx->matches         = NULL;
    ctx->unmatched       = NULL;
    ctx->unmatched_count = 0;
    ctx->buyers_count    = 0;
    ctx->buyers_max      = 0;
    ctx->items_count     = 0;
    ctx->items_max       = 0;
}

extern void
match_ctx_resize(match_ctx_t* ctx, size_t buyers_newmax, size_t items_newmax)
{
    ctx->payoffs = realloc(
        ctx->payoffs,
        buyers_newmax*items_newmax*sizeof(uint_fast32_t)
        );
    ctx->prices = realloc(
        ctx->prices,
        items_newmax*sizeof(double)
    );
    ctx->matches = realloc(
        ctx->matches,
        items_newmax*sizeof(size_t)
    );
    ctx->unmatched = realloc(
        ctx->unmatched,
        buyers_newmax*sizeof(size_t)
    );
    ctx->buyers_max = buyers_newmax;
    ctx->items_max  = items_newmax;
}

extern void
match_ctx_thd_resize(size_t buyers_newmax, size_t items_newmax)
{
    match_ctx_resize(
        match_ctx_thd_get(),
        buyers_newmax,
        items_newmax
        );
}

inline static item_preference_t
match_ctx_get_pref(match_ctx_t* ctx, size_t person)
{
    item_preference_t pref = { 0, 0 };
    float net;
    float fst_best_net = -INFINITY;
    float snd_best_net = -INFINITY;
    for (size_t i = 0; i < ctx->items_count; i++)
    {
        net = PAYOFF(ctx, person, i) - ctx->prices[i];
        if (net > fst_best_net) {
            snd_best_net = fst_best_net;
            pref.snd     = pref.fst;
            fst_best_net = net;
            pref.fst     = i;
        } else if (net > snd_best_net) {
            snd_best_net = net;
            pref.snd     = i;
        }
    }
    return pref;
}

inline static size_t
match_ctx_get_unmatched_person(match_ctx_t* ctx)
{
    return ctx->unmatched[--ctx->unmatched_count];
}

inline static void
match_ctx_match(match_ctx_t* ctx, size_t person, size_t item)
{
    if (ctx->matches[item] != NO_MATCH) {
        ctx->unmatched[ctx->unmatched_count++] = ctx->matches[item];
    }
    ctx->matches[item] = person;
}


extern uint_fast32_t
match_ctx_exec(match_ctx_t* ctx)
{
    size_t            person;
    item_preference_t pref;
    uint_fast32_t     total_weight = 0;

    while (ctx->unmatched_count > 0) {
        person = match_ctx_get_unmatched_person(ctx);
        pref   = match_ctx_get_pref(ctx, person);

        ctx->prices[pref.fst] += (
           +(PAYOFF(ctx, person, pref.fst) - ctx->prices[pref.fst])
           -(PAYOFF(ctx, person, pref.snd) - ctx->prices[pref.snd])
           +ctx->eps
        );
        match_ctx_match(ctx, person, pref.fst);
    }

    for (size_t i = 0; i < ctx->items_count; i++)
    {
        if (ctx->matches[i] != NO_MATCH) {
            total_weight += PAYOFF(ctx, ctx->matches[i], i);
        }
    }

    return total_weight;
}
