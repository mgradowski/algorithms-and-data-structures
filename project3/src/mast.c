#include <limits.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "compatibility.h"
#include "mast.h"
#include "match.h"
#include "tree.h"


static thread_local mast_ctx_t mast_ctx_thd = {
    .lookup_maxsize = 0,
    .lookup         = NULL,
    .t1             = NULL,
    .t2             = NULL,
    .match_ctx      = NULL
};

static uint_fast32_t
mast_ctx_lookup_or_eval(mast_ctx_t*,
                        node_t const* restrict,
                        node_t const* restrict);

extern mast_ctx_t*
mast_ctx_thd_get(void)
{
    return &mast_ctx_thd;
}

extern mast_ctx_t*
mast_ctx_alloc(void)
{
    return (mast_ctx_t*)malloc(sizeof(mast_ctx_t));
}

extern void
mast_ctx_init(mast_ctx_t* ctx)
{
    ctx->lookup_maxsize = 0;
    ctx->lookup         = NULL;
    ctx->t1             = NULL;
    ctx->t2             = NULL;
    ctx->match_ctx      = match_ctx_alloc();
    match_ctx_init(ctx->match_ctx);
}

extern void
mast_ctx_thd_init(void)
{
    mast_ctx_thd_get()->match_ctx = match_ctx_thd_get();
}

extern void
mast_ctx_resize(mast_ctx_t* ctx, size_t newsize)
{
    ctx->lookup         = realloc(ctx->lookup, newsize*sizeof(size_t));
    ctx->lookup_maxsize = newsize;
}

extern void
mast_ctx_thd_resize(size_t newsize)
{
    mast_ctx_resize(mast_ctx_thd_get(), newsize);
}

extern void
mast_ctx_setup(mast_ctx_t* ctx,
               tree_t const* restrict t1,
               tree_t const* restrict t2)
{
    if (t1->leaf_count != t2->leaf_count) {
        fprintf(stderr, "Error: Trees must have an equal number of leaves.");
        exit(EXIT_FAILURE);
    }

    const size_t size_req = t1->node_count * t2->node_count;

    if (size_req > ctx->lookup_maxsize) {
        mast_ctx_resize(ctx, size_req);
    }
    
    for (size_t i = 0; i < size_req; i++) {
        ctx->lookup[i] = UNEVALUATED;
    }
    
    ctx->t1 = t1;
    ctx->t2 = t2;
}

extern void
mast_ctx_thd_setup(tree_t const* restrict t1, tree_t const* restrict t2)
{
    mast_ctx_setup(mast_ctx_thd_get(), t1, t2);
}

static void
mast_ctx_setup_match_ctx(mast_ctx_t* ctx,
                         node_t const* restrict n1,
                         node_t const* restrict n2)
{
    node_t const*          tmp_node;
    node_t const* restrict c1;
    node_t const* restrict c2;
    uint_fast32_t          tmp_count;
    size_t                 i;
    size_t                 j;
    bool                   swapped = false;

    ctx->match_ctx->buyers_count = node_count_children(n1);
    ctx->match_ctx->items_count  = node_count_children(n2);

    // The auction algorithm doesn't work if there are more buyers than items.
    if (ctx->match_ctx->buyers_count > ctx->match_ctx->items_count) {
        swapped = true;
        // Swap nodes.
        tmp_node = n2;
        n2       = n1;
        n1       = tmp_node;
        tmp_node = NULL;
        // Swap counts.
        tmp_count                    = ctx->match_ctx->buyers_count;
        ctx->match_ctx->buyers_count = ctx->match_ctx->items_count;
        ctx->match_ctx->items_count  = tmp_count;
    }

    // Resize the match context if too small.
    if (ctx->match_ctx->buyers_max < ctx->match_ctx->buyers_count
     || ctx->match_ctx->items_max  < ctx->match_ctx->items_count) {
         match_ctx_resize(
            ctx->match_ctx,
            ctx->match_ctx->buyers_count,
            ctx->match_ctx->items_count);
     }

    // Fill payoff matrix.
    c1 = n1->child;
    i = 0;
    do
    {
        c2 = n2->child;
        j = 0;
        do
        {
            PAYOFF(ctx->match_ctx, i, j) = swapped ?
                  mast_ctx_lookup_or_eval(ctx, c2, c1)
                : mast_ctx_lookup_or_eval(ctx, c1, c2);
            c2 = c2->sibling;
            j++;
        } while (c2 != NULL);
        c1 = c1->sibling;
        i++;
    } while (c1 != NULL);

    // Initialize prices and matches.
    for (i = 0; i < ctx->match_ctx->items_count; i++)
    {
        ctx->match_ctx->prices[i]  = 0;
        ctx->match_ctx->matches[i] = NO_MATCH;
    }

    // Initialize the stack of unmatched buyers.
    for (i = 0; i < ctx->match_ctx->buyers_count; i++)
    {
        ctx->match_ctx->unmatched[i] = i;
    }
    ctx->match_ctx->unmatched_count = ctx->match_ctx->buyers_count;
    ctx->match_ctx->eps = 1. / (
        ctx->match_ctx->buyers_count + ctx->match_ctx->items_count
        );
}

static void
mast_ctx_thd_setup_matching(node_t const* n1, node_t const* n2)
{
    mast_ctx_setup_match_ctx(mast_ctx_thd_get(), n1, n2);
}

static void
mast_ctx_eval_leaves(mast_ctx_t* ctx)
{
    size_t  i;
    size_t  j;
    node_t* n;
    // Leaf <-> Leaf
    for (i = 0; i < ctx->t1->leaf_count; i++)
    {
        for (j = 0; j < ctx->t2->leaf_count; j++)
        {
            MAST_CTX_LOOKUP(ctx, i, j) = i == j ? 1 : 0;
        }        
    }
    // Leaf <-> Node
    for (i = 0; i < ctx->t1->leaf_count; i++)
    {
        for (j = ctx->t2->leaf_count; j < ctx->t2->node_count; j++)
        {
            MAST_CTX_LOOKUP(ctx, i, j) = 0;
        }
        
        n = ctx->t2->node_lookup[i];
        while(!IS_ROOT(n)) {
            n = n->parent;
            MAST_CTX_LOOKUP(ctx, i, n->value) = 1;
        }
        
    }
    // Node <-> Leaf
    for (j = 0; j < ctx->t2->leaf_count; j++)
    {
        for (i = ctx->t1->leaf_count; i < ctx->t1->node_count; i++)
        {
            MAST_CTX_LOOKUP(ctx, i, j) = 0;
        }
        
        n = ctx->t1->node_lookup[j];
        while(!IS_ROOT(n)) {
            n = n->parent;
            MAST_CTX_LOOKUP(ctx, n->value, j) = 1;
        }
    }   
}

static void
mast_ctx_thd_eval_leaves(void)
{
    mast_ctx_eval_leaves(mast_ctx_thd_get());
}

static uint_fast32_t
mast_ctx_lookup_or_eval(mast_ctx_t* ctx,
                        node_t const* restrict n1,
                        node_t const* restrict n2)
{
    node_t const* restrict c;
    uint_fast32_t          max;
    uint_fast32_t          tmp;
    // Check if already evaluated.
    tmp = MAST_CTX_LOOKUP(ctx, n1->value, n2->value);
    if (tmp != UNEVALUATED) {
        return tmp;
    }
    max = 0;
    // 1st case.
    c = n1->child;
    do
    {
        tmp = mast_ctx_lookup_or_eval(ctx, c, n2);
        if (tmp > max) {
            max = tmp;
        }
        c = c->sibling;
    } while (c != NULL);
    // 2nd case.
    c = n2->child;
    do
    {
        tmp = mast_ctx_lookup_or_eval(ctx, n1, c);
        if (tmp > max) {
            max = tmp;
        }
        c = c->sibling;
    } while (c != NULL);
    // 3rd case.
    mast_ctx_setup_match_ctx(ctx, n1, n2);
    tmp = match_ctx_exec(ctx->match_ctx);
    if (tmp > max) {
        max = tmp;
    }
    MAST_CTX_LOOKUP(ctx, n1->value, n2->value) = max;
    return max;
}

static uint_fast32_t
mast_ctx_thd_lookup_or_eval(node_t const* restrict n1,
                            node_t const* restrict n2)
{
    return mast_ctx_lookup_or_eval(mast_ctx_thd_get(), n1, n2);
}

extern uint_fast32_t
mast(mast_ctx_t* ctx, tree_t const* restrict t1, tree_t const* restrict t2)
{
    mast_ctx_setup(ctx, t1, t2);
    mast_ctx_eval_leaves(ctx);
    return mast_ctx_lookup_or_eval(ctx, t1->root, t2->root);
}

extern uint_fast32_t
mast_thd(tree_t const* restrict t1, tree_t const* restrict t2) {
    return mast(mast_ctx_thd_get(), t1, t2);
}
