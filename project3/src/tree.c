#include <inttypes.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "compatibility.h"
#include "tree.h"


static inline int
fpeek(FILE* f)
{
    int c = fgetc(f);
    ungetc(c, f);
    return c;
}

static node_t*
node_alloc(void)
{
    return (node_t*) malloc(sizeof(node_t));
}

static void
node_init(node_t* node)
{
    node->child   = NULL;
    node->parent  = NULL;
    node->sibling = NULL;
    node->value   = 0;
}

static void
node_add_child(node_t* restrict p, node_t* restrict c)
{
    c->parent = p;

    if (p->child == NULL) {
        p->child = c;
        return;
    }

    p = p->child;

    while(p->sibling != NULL) {
        p = p->sibling;
    }

    p->sibling = c;
}

static size_t
node_label_inner(node_t* node, size_t from)
{
    node_t* c;

    if (IS_LEAF(node)) {
        return from;
    }

    c = node->child;

    do {
        from = node_label_inner(c, from);
        c = c->sibling;
    } while (c != NULL);

    node->value = from++;

    return from;
}

extern size_t
node_count_children(node_t const* node)
{
    node_t* c;
    size_t cnt;

    if (IS_LEAF(node)) {
        return 0;
    }

    c = node->child;
    cnt = 0;

    do {
        cnt += 1;
        c = c->sibling;
    } while (c != NULL);
    return cnt;
}

static void
node_fprint(FILE* restrict f, node_t const* restrict node)
{
    node_t const* restrict c;

    if (IS_LEAF(node)) {
        fprintf(f, "%" PRIval, node->value);
        return;
    }

    c = node->child;

    fputs("(", f);
    node_fprint(f, c);

    while (c->sibling != NULL) {
        c = c->sibling;
        fputs(",", f);
        node_fprint(f, c);
    }
    
    fputs(")", f);
}

static node_t*
node_fscan(FILE* restrict f, tree_t* tree)
{
    node_t* restrict leaf;
    char c = 0;

    fgetc(f); // Skip opening parenthesis

    node_t* restrict node = node_alloc();
    node_init(node);

    for(;;)
    {
        c = fpeek(f);
        switch (c)
        {
        case ')':
            getc(f); // Skip closing parenthesis
            tree->node_count++;
            return node;
            break;
        case '(':
            node_add_child(node, node_fscan(f, tree));
            break;
        case ',':
            fgetc(f); // Skip a comma
            break;
        default:
            leaf = node_alloc();
            node_init(leaf);
            fscanf(f, " %" SCNval, &leaf->value);
            node_add_child(node, leaf);

            leaf->value -= 1;
            tree->leaf_count++;
            tree->node_count++;
            break;
        }
        fscanf(f, " ");
    }

    return NULL;
}

static tree_t*
tree_alloc(void)
{
    return (tree_t*) malloc(sizeof(tree_t));
}

extern void
tree_free(tree_t* tree)
{
    for (size_t i = 0; i < tree->node_count; i++)
    {
        free(tree->node_lookup[i]);
    }
    
    free(tree->node_lookup);
    free(tree);
}

static void
tree_init(tree_t* tree)
{
    tree->root        = NULL;
    tree->node_lookup = NULL;
    tree->leaf_count  = 0;
    tree->node_count  = 0;
}

static void
tree_label_inner_nodes(tree_t* tree)
{
    node_label_inner(tree->root, tree->leaf_count);
}

static void
tree_fill_lookup(tree_t* tree, node_t* node) {
    node_t* c;
    tree->node_lookup[node->value] = node;

    if (IS_LEAF(node)) {
        return;
    }
    
    c = node->child;
    do {
        tree_fill_lookup(tree, c);
        c = c->sibling;
    } while (c != NULL);
}

extern tree_t*
tree_fscan(FILE* restrict f)
{
    // Skip whitespace
    fscanf(f, " ");

    tree_t* tree = tree_alloc();
    tree_init(tree);

    tree->root = node_fscan(f, tree);
    tree_label_inner_nodes(tree);
    // Skip the semicolon
    fgetc(f);
    tree->node_lookup = (node_t**)malloc(tree->node_count*sizeof(node_t*));
    tree_fill_lookup(tree, tree->root);
    return tree;
}

extern tree_t*
tree_scan(void)
{
    return tree_fscan(stdin);
}

extern void
tree_fprint(FILE* restrict f, tree_t const* tree)
{
    node_fprint(f, tree->root);
    fputs(";", f);
}

extern void
tree_print(tree_t const* tree)
{
    tree_fprint(stdout, tree);
}
