#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>


typedef size_t symbol_t;

typedef struct {
    size_t    size;
    symbol_t* definition;
} permutation_t;

typedef struct {
    size_t               size;
    permutation_t const* permutation;
    permutation_t const* permutation_inv;
    size_t               notch_count;
    size_t*              notches;
} rotor_t;

typedef struct {
    rotor_t const* rotor;
    size_t         position;
} rotor_state_t;

typedef struct {
    size_t               rotor_count;
    size_t               alphabet_size;
    rotor_state_t**      rotors;
    permutation_t const* reflector;
    bool                 first_moved;  // STOS workaround flags.
    bool                 second_moved;
} encoder_t;

typedef enum {
    FORWARD,
    BACKWARD
} direction_t;


static permutation_t*
permutation_malloc(size_t size)
{
    permutation_t* ptr;
    ptr = (permutation_t*) malloc(sizeof(permutation_t));
    ptr->definition = (symbol_t*) malloc(size * sizeof(symbol_t));
    ptr->size = size;
    return ptr;
}

extern void
permutation_free(permutation_t* ptr)
{
    free(ptr->definition);
    free(ptr);
}

extern permutation_t const*
permutation_invert(permutation_t const* p)
{
    permutation_t* p_inv = permutation_malloc(p->size);

    for (size_t i = 0; i < p->size; i++)
    {
        p_inv->definition[p->definition[i]] = i;
    }
    
    return p_inv;
}


extern permutation_t const*
permutation_fscan(FILE* stream, size_t size)
{
    permutation_t* p = permutation_malloc(size);

    for (size_t i = 0; i < size; i++)
    {
        fscanf(stream, " %zu", &p->definition[i]);
        p->definition[i] -= 1;
    }
    
    return p;
}

extern permutation_t const*
permutation_scan(size_t size)
{
    return permutation_fscan(stdin, size);
}

extern symbol_t
permutation_apply(permutation_t const* p, symbol_t s)
{
    return p->definition[s];
}

extern rotor_t*
rotor_malloc(size_t notch_count)
{
    rotor_t* ptr = (rotor_t*) malloc(sizeof(rotor_t));
    ptr->notch_count = notch_count;
    ptr->notches = notch_count > 0 ? (size_t*) malloc(notch_count * sizeof(size_t))
                                   : NULL;
    return ptr;
}

static bool
rotor_at_notch(rotor_state_t const* rs)
{
    for (size_t i = 0; i < rs->rotor->notch_count; i++)
    {
        if (rs->position == rs->rotor->notches[i]) {
            return true;
        }
    }
    
    return false;
}

static rotor_state_t*
rotor_state_malloc(void) {
    return (rotor_state_t*) malloc(sizeof(rotor_state_t));
}

static void
rotor_state_free(rotor_state_t* ptr) {
    free(ptr);
}

static rotor_state_t*
rotor_state_new(rotor_t const* rotor, size_t initial_position) {
    rotor_state_t* rs = rotor_state_malloc();
    rs->rotor         = rotor;
    rs->position      = initial_position;

    return rs;
}

static symbol_t
rotor_state_pass_static(rotor_state_t const* rs, symbol_t s, direction_t d)
{
    s += rs->position;
    s %= rs->rotor->size;
    s  = permutation_apply(d == FORWARD ? rs->rotor->permutation
                                        : rs->rotor->permutation_inv, s);
    // In C, a % b for negative numbers is not the same as a mod b.
    s += rs->rotor->size;
    s -= rs->position;
    s %= rs->rotor->size;
    
    return s;
}

extern encoder_t*
encoder_malloc(size_t rotor_count)
{
    encoder_t* ptr = (encoder_t*) malloc(sizeof(encoder_t));
    ptr->rotor_count = rotor_count;
    ptr->rotors = rotor_count > 0 ? (rotor_state_t**) malloc(rotor_count * sizeof(rotor_state_t*))
                                  : NULL;
    ptr->first_moved  = false;
    ptr->second_moved = false;

    return ptr;
}

extern void
encoder_free(encoder_t* ptr)
{
    if (ptr->rotor_count > 0) {
        free(ptr->rotors);
    }
    free(ptr);
}

static void
encoder_advance_rotors(encoder_t* e)
{
    if (e->rotor_count >= 3 && rotor_at_notch(e->rotors[1]) && e->second_moved)
        goto advance_all;
    
    if (e->rotor_count >= 2 && rotor_at_notch(e->rotors[0]) && e->first_moved)
        goto advance_two;

    goto advance_one;

advance_all:
    e->rotors[2]->position += 1;
    e->rotors[2]->position %= e->alphabet_size;
advance_two:
    e->rotors[1]->position += 1;
    e->rotors[1]->position %= e->alphabet_size;
    e->second_moved = true;
advance_one:
    e->rotors[0]->position += 1;
    e->rotors[0]->position %= e->alphabet_size;
    e->first_moved = true;
}

static symbol_t
encoder_encode_static(encoder_t const* e, symbol_t s)
{
    size_t i;

    for (i = 0; i < e->rotor_count; i++)
    {
        s = rotor_state_pass_static(e->rotors[i], s, FORWARD);
    }

    s = permutation_apply(e->reflector, s);

    for (i = 0; i < e->rotor_count; i++)
    {
        s = rotor_state_pass_static(e->rotors[e->rotor_count - (i + 1)], s, BACKWARD);
    }

    return s;
}

extern symbol_t
encoder_encode(encoder_t* e, symbol_t s)
{
    encoder_advance_rotors(e);
    return encoder_encode_static(e, s);
}

extern rotor_t const*
rotor_fscan(FILE* stream, size_t size)
{
    size_t notch_count;
    rotor_t* rot;
    permutation_t const* perm = permutation_fscan(stream, size);

    scanf(" %zu", &notch_count);
    rot = rotor_malloc(notch_count);
    rot->size = perm->size;
    rot->permutation = perm;
    rot->permutation_inv = permutation_invert(perm);

    for (size_t i = 0; i < notch_count; i++)
    {
        scanf(" %zu", &rot->notches[i]);
        rot->notches[i] += size - 2;
        rot->notches[i] %= size;
    }
    
    return rot;
}

extern rotor_t const*
rotor_scan(size_t size)
{
    return rotor_fscan(stdin, size);
}

extern symbol_t
symbol_fscan(FILE* stream)
{
    symbol_t s;
    fscanf(stream, "%zu", &s);
    return s;
}

extern symbol_t
symbol_scan(void)
{
    return symbol_fscan(stdin);
}

extern void
symbol_fprint(FILE* stream, symbol_t s)
{
    fprintf(stream, "%zu ", s);
}

extern void
symbol_print(symbol_t s) {
    symbol_fprint(stdout, s);
}

int
main(void) {
    // Scanf buffers.
    size_t alphabet_length;
    size_t total_rotor_count;
    size_t total_reflector_count;
    size_t rotor_count;
    size_t task_count;
    size_t rotor_index;
    size_t rotor_init_pos;
    size_t reflector_index;
    symbol_t s;

    // Loop indices.
    size_t i;
    size_t j;
 
    const rotor_t**       rotors;
    const permutation_t** reflectors;
    encoder_t*            enc;

    scanf(" %zu", &alphabet_length);

    // Scan rotors.
    scanf(" %zu", &total_rotor_count);
    rotors = (const rotor_t**) malloc(total_rotor_count * sizeof(rotor_t const*));
    for (i = 0; i < total_rotor_count; i++)
    {
        rotors[i] = rotor_scan(alphabet_length);
    }
    
    // Scan reflectors.
    scanf(" %zu", &total_reflector_count);
    reflectors = (const permutation_t**) malloc(total_reflector_count * sizeof(permutation_t const*));
    for (i = 0; i < total_reflector_count; i++)
    {
        reflectors[i] = permutation_scan(alphabet_length);
    }

    // Do tasks.
    scanf(" %zu", &task_count);
    for (i = 0; i < task_count; i++)
    {
        scanf(" %zu", &rotor_count);

        enc = encoder_malloc(rotor_count);
        enc->alphabet_size = alphabet_length;
        for (j = 0; j < rotor_count; j++)
        {
            scanf(" %zu", &rotor_index);
            scanf(" %zu", &rotor_init_pos);

            rotor_init_pos += alphabet_length;
            rotor_init_pos -= 1;
            rotor_init_pos %= alphabet_length;

            enc->rotors[j] = rotor_state_new(rotors[rotor_index],
                                             rotor_init_pos);
        }

        scanf(" %zu", &reflector_index);
        enc->reflector = reflectors[reflector_index];
        
        while ((s = symbol_scan()) != 0)
        {
            symbol_print(encoder_encode(enc, s-1) + 1);
        }
        printf("\n");

        for (j = 0; j < rotor_count; j++)
        {
            rotor_state_free(enc->rotors[j]);
        }
        encoder_free(enc);
    }
    
    return 0;
}