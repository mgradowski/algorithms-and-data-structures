#include <stdio.h>
#include <stdlib.h>


typedef enum
{
  INCREASING,
  DECREASING
} monotonicity_t;


int*
find(int* start, int* end, int value, monotonicity_t monotonicity)
{
  int* mid = start + (end - start) / 2;

  if (start == end && *start != value) {
    return NULL;
  }

  if (*mid == value) {
    return mid;
  }

  switch (monotonicity) {
    case INCREASING:
      if (*mid > value) {
        return find(start  , mid, value, monotonicity);
      } else {
        return find(mid + 1, end, value, monotonicity);
      }
      break;
    case DECREASING:
      if (*mid > value) {
        return find(mid + 1, end, value, monotonicity);
      } else {
        return find(start,   mid, value, monotonicity);
      }
      break;
  }
  
  return NULL;
}

int
main(void)
{
  // Scanf buffers.
  unsigned int cases;
  unsigned int all_size;
  unsigned int target_count;

  int* all;
  int  target_value;
  int* target_ptr;
  int  target_index;

  scanf(" %u", &cases);
  for (int i = 0; i < cases; i++) {
    scanf(" %u", &all_size);

    all = (int*)malloc(all_size * sizeof(int));
    for (int i = 0; i < all_size; i++) {
      scanf(" %d", &all[i]);
    }

    monotonicity_t monotonicity =
      all_size > 1 && all[0] > all[1] ? DECREASING : INCREASING;

    scanf(" %u", &target_count);

    for (int i = 0; i < target_count; i++) {
      scanf(" %u", &target_value);

      target_ptr = find(all, &all[all_size - 1], target_value, monotonicity);
      target_index = target_ptr!= NULL ? target_ptr - all : -1;

      printf("%d\n", target_index);
    }

    free(all);
  }

  return 0;
}