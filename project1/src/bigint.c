#include <ctype.h>
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>


// Byte multiples.
#define BYTE 1
#define K_BYTE (1024 *   BYTE)
#define M_BYTE (1024 * K_BYTE)

// Scan buffer constants.
#define INITIAL_BUFFER_SIZE   (4   * K_BYTE)
#define DOUBLING_UPPER_BOUND  (1   * M_BYTE)
#define BUFFER_SIZE_INCREMENT (512 * K_BYTE)

// Stringify macros.
#define str(s) _str(s)
#define _str(s) #s

// Radix constants.
#define DIGITS_PER_CHUNK 17 // from range [1, 17]
#define DIGITS_PER_CHUNK_STR str(DIGITS_PER_CHUNK)
#define RADIX (uint64_t)(pow(10, DIGITS_PER_CHUNK))

#define CMD_BUF_LEN 64
#define CMD_BUF_LEN_STR str(CMD_BUF_LEN)
#define NUM_ARR_LEN 100000

#ifdef DEBUG
  #define D_PRINTF(...) fprintf(stderr, __VA_ARGS__ );
#else
  #define D_PRINTF(...)
#endif


#ifndef max
  #define max(a, b) ((a) > (b) ? (a) : (b))
#endif


typedef int64_t chunk_t;

typedef enum
{
  POSITIVE,
  NEGATIVE
} sign_t;

typedef struct
{
  size_t size;
  chunk_t* chunks;
  sign_t sign;
} bigint_t;

static __thread struct
{
  uint64_t size;
  char* buf;
} scan_buffer = { .size = 0, .buf = NULL };

static void
scan_buffer_init(void)
{
  scan_buffer.buf = malloc(INITIAL_BUFFER_SIZE);
  scan_buffer.size = INITIAL_BUFFER_SIZE;
}

static void
scan_buffer_grow(void)
{
  uint64_t new_size = scan_buffer.size < DOUBLING_UPPER_BOUND
                        ? 2 * scan_buffer.size
                        : scan_buffer.size + BUFFER_SIZE_INCREMENT;
  scan_buffer.buf = realloc(scan_buffer.buf, new_size);
  scan_buffer.size = new_size;
}

size_t
bigint_count_nonzero_chunks(bigint_t* bint)
{
  for (size_t i = 0; i < bint->size; i++)
  {
    if (bint->chunks[bint->size - (i + 1)]) {
      return bint-> size - i;
    }
  }
  return 0;
}

static void
scan_buffer_free(void)
{
  free(scan_buffer.buf);
  scan_buffer.size = 0;
}

extern bigint_t*
bigint_malloc(size_t size)
{
  bigint_t* bint = malloc(sizeof(bigint_t));
  bint->chunks = calloc(size, sizeof(chunk_t));
  bint->size = size;
  return bint;
}

extern void
bigint_free(bigint_t* ptr)
{
  free(ptr->chunks);
  free(ptr);
}

static void
bigint_fscan_buffer(FILE* stream)
{
  uint64_t i = 0;
  char c;

  // Skip shitespace.
  fscanf(stream, " ");

  while (isdigit(c = getc(stream))) {
    if (i >= scan_buffer.size - 2) {
      scan_buffer_grow();
    }
    scan_buffer.buf[i++] = c;
  }

  if (!isspace(c)) {
    // Leading whitespace messes with some other input functions.
    ungetc(c, stream);
  }
  
  scan_buffer.buf[i] = '\0';
}

static bigint_t*
bigint_from_string(char* s)
{
  uint64_t chunk;
  uint64_t digits_left;

  uint64_t num_digits = strlen(s);
  uint64_t num_chunks = num_digits / DIGITS_PER_CHUNK + 1;

  uint64_t i = 0;
  bigint_t* bint = bigint_malloc(num_chunks);
  bint->sign = POSITIVE;

  // Load digits chunk by chunk.
  for (digits_left = num_digits; digits_left > DIGITS_PER_CHUNK;
       digits_left -= DIGITS_PER_CHUNK) {
    sscanf(&scan_buffer.buf[digits_left - DIGITS_PER_CHUNK],
           "%" DIGITS_PER_CHUNK_STR SCNu64,
           &chunk);
    bint->chunks[i++] = chunk;
  }

  // Most significant chunk may have less that DIGITS_PER_CHUNK digits.
  if (digits_left > 0) {
    scan_buffer.buf[digits_left] = '\0';
    sscanf(scan_buffer.buf, "%" SCNu64, &chunk);
    bint->chunks[i] = chunk;
  }
  return bint;
}

extern void
bigint_fprint(bigint_t* bint, FILE* stream)
{
  if (bint->sign == NEGATIVE) {
    fputs("-", stream);
  }

  if (bint->size == 1) {
    fprintf(stream, "%" PRIi64, bint->chunks[0]);
    return;
  }

  // Skip leading zeros.
  int64_t i_start = bigint_count_nonzero_chunks(bint) - 1;
  
  // Most significant chunk is left unpadded.
  fprintf(stream, "%" PRIi64, bint->chunks[i_start]);

  // The rest are padded with zeros.
  for (int64_t i = i_start - 1; i >= 0; i--) {
    fprintf(stream, "%0" DIGITS_PER_CHUNK_STR PRIi64, bint->chunks[i]);
  }
}

extern void
bigint_print(bigint_t* bint)
{
  bigint_fprint(bint, stdout);
}

extern bigint_t*
bigint_fscan(FILE* stream)
{
  sign_t sign;
  char c;

  // Skip whitespace.
  fscanf(stream, " ");


  switch (c = getc(stream))
  {
  case '-':
    sign = NEGATIVE;
    break;
  case '+':
    sign = POSITIVE;
    break;

  default:
    sign = POSITIVE;
    ungetc(c, stream);
    break;
  }

  bigint_fscan_buffer(stream);
  bigint_t* result = bigint_from_string(scan_buffer.buf);
  result->sign = sign;

  return result;
}

extern bigint_t*
bigint_scan(void)
{
  return bigint_fscan(stdin);
}

extern bigint_t*
bigint_from_int(int64_t n)
{
  bigint_t* result = bigint_malloc(1);

  result->chunks[0] = n >= 0 ? n : -n;
  result->sign = n >= 0 ? POSITIVE : NEGATIVE;

  return result;
}

static bool
bigint_gte_abs(bigint_t* l, bigint_t* r)
{
  uint64_t size = max(l->size, r->size);
  chunk_t l_chunk;
  chunk_t r_chunk;
  
  // Compare chunk by chunk, starting from the most significant chunks.
  for (int64_t i = size - 1; i >= 0; i--) {

    l_chunk = i < l->size ? l->chunks[i] : 0;
    r_chunk = i < r->size ? r->chunks[i] : 0;

    if (l_chunk > r_chunk) {
      return true;
    }

    if (l_chunk < r_chunk) {
      return false;
    }
  }

  return true;
}

static bool
bigint_eq_abs(bigint_t* l, bigint_t* r)
{
  uint64_t size = max(l->size, r->size);
  chunk_t l_chunk;
  chunk_t r_chunk;
  
  // Compare chunk by chunk.
  for (int64_t i = size - 1; i >= 0; i--) {
    l_chunk = i < l->size ? l->chunks[i] : 0;
    r_chunk = i < r->size ? r->chunks[i] : 0;

    if (l_chunk != r_chunk) {
      return false;
    }
  }

  return true;
}

static bigint_t*
bigint_add_positive(bigint_t* bigger, bigint_t* smaller, sign_t result_sign)
{
  uint64_t result_size = max(bigint_count_nonzero_chunks(bigger), bigint_count_nonzero_chunks(smaller)) + 1;
  bigint_t* result = bigint_malloc(result_size);

  chunk_t carry = 0;
  chunk_t bigger_chunk;
  chunk_t smaller_chunk;

  for (uint64_t i = 0; i < result_size; i++) {
    bigger_chunk  = i <  bigger->size ? bigger->chunks[i]  : 0;
    smaller_chunk = i < smaller->size ? smaller->chunks[i] : 0;
    result->chunks[i] = bigger_chunk + smaller_chunk + carry;

    carry = result->chunks[i] / RADIX;
    result->chunks[i] %= RADIX;
  }

  result->sign = result_sign;

  return result;
}

static bigint_t*
bigint_sub_positive(bigint_t* bigger, bigint_t* smaller, sign_t result_sign)
{
  uint64_t size = max(bigger->size, smaller->size);
  bigint_t* result = bigint_malloc(size);

  chunk_t bigger_chunk;
  chunk_t smaller_chunk;

  if (bigint_eq_abs(bigger, smaller)) {
    return bigint_from_int(0);
  }

  // Subtract chunk by chunk.
  for (uint64_t i = 0; i < size; i++) {
    bigger_chunk  = i <  bigger->size ? bigger->chunks[i]  : 0;
    smaller_chunk = i < smaller->size ? smaller->chunks[i] : 0;
    result->chunks[i] = bigger_chunk - smaller_chunk;
  }

  // If resultant chunk is negative, borrow from the bigger adjacent chunk.
  for (uint64_t i = 0; i < size - 1; i++) {
    if (result->chunks[i] < 0) {
      result->chunks[i] += RADIX;
      result->chunks[i + 1] -= 1;
    }
  }

  result->sign = result_sign;

  return result;
}

extern bool
bigint_eq(bigint_t* l, bigint_t* r)
{
  return l->sign == r-> sign && bigint_eq_abs(l, r);
}

extern bigint_t*
bigint_add(bigint_t* l, bigint_t* r)
{
  bool flip_sign = false;

  if (!bigint_gte_abs(l, r)) {
    flip_sign = true;
  }

  if (l->sign == POSITIVE && r->sign == POSITIVE) {
    return bigint_add_positive(l, r, POSITIVE);
  }

  if (l->sign == POSITIVE && r->sign == NEGATIVE) {
    if (flip_sign) {
      return bigint_sub_positive(r, l, NEGATIVE);
    }
    else {
      return bigint_sub_positive(l, r, POSITIVE);
    }
  }

  if (l->sign == NEGATIVE && r->sign == POSITIVE) {
    if (flip_sign) {
      return bigint_sub_positive(r, l, POSITIVE);
    }
    else {
      return bigint_sub_positive(l, r, NEGATIVE);
    }
  }

  if (l->sign == NEGATIVE && r->sign == NEGATIVE) {
    return bigint_add_positive(l, r, NEGATIVE);
  }

  return NULL;
}

extern bigint_t*
bigint_sub(bigint_t* l, bigint_t* r)
{
  bool flip_sign = false;

  if (!bigint_gte_abs(l, r)) {
    flip_sign = true;
  }

  if (l->sign == POSITIVE && r->sign == POSITIVE) {
    if(flip_sign) {
      return bigint_sub_positive(r, l, NEGATIVE);
    }
    else {
      return bigint_sub_positive(l, r, POSITIVE);
    }
  }

  if (l->sign == POSITIVE && r->sign == NEGATIVE) {
    return bigint_add_positive(l, r, POSITIVE);
  }

  if (l->sign == NEGATIVE && r->sign == POSITIVE) {
    return bigint_add_positive(l, r, NEGATIVE);
  }

  if (l->sign == NEGATIVE && r->sign == NEGATIVE) {
    if(flip_sign) {
      return bigint_sub_positive(r, l, POSITIVE);
    }
    else {
      return bigint_sub_positive(l, r, NEGATIVE);
    }
  }

  return NULL;
}

extern bool
bigint_gte(bigint_t* l, bigint_t* r)
{
  bool l_gte_abs = bigint_gte_abs(l, r);

  if (l->sign == POSITIVE && r->sign == POSITIVE) {
    if (l_gte_abs) {
      return true;
    }
    else {
      return bigint_eq_abs(l, r);
    }
  }

  if (l->sign == POSITIVE && r->sign == NEGATIVE) {
    return true;
  }

  if (l->sign == NEGATIVE && r->sign == POSITIVE) {
    return false;
  }

  if (l->sign == NEGATIVE && r->sign == NEGATIVE) {
    if (l_gte_abs) {
      return bigint_eq_abs(l, r);
    }
    else {
      return true;
    }
  }

  return false;
}

int
main(void)
{
  bigint_t** num_arr = calloc(NUM_ARR_LEN, sizeof(bigint_t*));
  bigint_t*  min;
  bigint_t*  max;
  bigint_t*  old;

  uint32_t numbers_scanned = 0;
  uint32_t numbers_to_scan;

  // Scanf buffers for reading indices.
  uint32_t i;
  uint32_t j;
  uint32_t k;

  char cmd_buf[CMD_BUF_LEN] = { '\0' };

  scan_buffer_init();

  for(;;) {
    // Read command and remove trailing newline.
    fgets(cmd_buf, CMD_BUF_LEN, stdin);
    cmd_buf[strcspn(cmd_buf, "\n")] = '\0';

    D_PRINTF("cmd_buf(\"%s\")\n", cmd_buf);

    // Quit.
    if (strncmp(cmd_buf, "q", CMD_BUF_LEN)==0) {
      D_PRINTF("Quitting.\n");
      break;
    }
    // Print all.
    else if (strncmp(cmd_buf, "?", CMD_BUF_LEN)==0) {
      D_PRINTF("Printing all.\n");
      for (uint64_t i = 0; i < numbers_scanned; i++)
      {
        bigint_print(num_arr[i]);
        printf("\n");
      }
    }
    // Print min.
    else if (strncmp(cmd_buf, "min", CMD_BUF_LEN)==0 && numbers_scanned > 0) {
      D_PRINTF("Printing min.\n");
      min = num_arr[0];
      for (uint64_t i = 1; i < numbers_scanned; i++)
      {
        if (bigint_gte(min, num_arr[i])) {
          min = num_arr[i];
        }
      }

      bigint_print(min);
      printf("\n");
    }
    // Print max.
    else if (strncmp(cmd_buf, "max", CMD_BUF_LEN)==0 && numbers_scanned > 0) {
      D_PRINTF("Printing max.\n");
      max = num_arr[0];
      for (uint64_t i = 1; i < numbers_scanned; i++)
      {
        if (bigint_gte(num_arr[i], max)) {
          max = num_arr[i];
        }
      }

      bigint_print(max);
      printf("\n");
    }
    // Add.
    else if (sscanf(cmd_buf, "%" SCNu32 " = " "%" SCNu32 " + %" SCNu32, &i, &j, &k)==3) {
      if (   i > numbers_scanned - 1
          || j > numbers_scanned - 1
          || k > numbers_scanned - 1) {
            fputs("Array index out of bound! Exitting.\n", stderr);
            exit(EXIT_FAILURE);
      }
      D_PRINTF("arr[%" PRIu32 "] + arr[%" PRIu32 "] -> store at index %" PRIu32".\n", j, k, i);
      old = num_arr[i];
      num_arr[i] = bigint_add(num_arr[j], num_arr[k]);
      D_PRINTF("Result size: %lu\n", num_arr[i]->size);
      bigint_free(old);
    }
    // Subtract.
    else if (sscanf(cmd_buf, "%" SCNu32 " = " "%" SCNu32 " - %" SCNu32, &i, &j, &k)==3) {
      if (   i > numbers_scanned - 1
          || j > numbers_scanned - 1
          || k > numbers_scanned - 1) {
            fputs("Array index out of bound! Exitting.\n", stderr);
            exit(EXIT_FAILURE);
      }
      D_PRINTF("arr[%" PRIu32 "] + arr[%" PRIu32 "] -> store at index %" PRIu32".\n", j, k, i);

      old = num_arr[i];
      num_arr[i] = bigint_sub(num_arr[j], num_arr[k]);
      bigint_free(old);
    }
    // Scan.
    else if (sscanf(cmd_buf, "%" SCNu32 "\n", &numbers_to_scan)==1) {
      D_PRINTF("Loading %" PRIu32" numbers.\n", numbers_to_scan);
      for (uint32_t i = 0; i < numbers_to_scan; i++)
      {
        num_arr[numbers_scanned++] = bigint_scan();
      }
    }
    // Error.
    else {
      fputs("Invalid input sequence! Exiting.\n", stderr);
      exit(EXIT_FAILURE);
    }
  }

   
  for (size_t i = 0; i < numbers_scanned; i++)
  {
    bigint_free(num_arr[i]);
  }

  free(num_arr);
  scan_buffer_free();
  
  return 0;
}
