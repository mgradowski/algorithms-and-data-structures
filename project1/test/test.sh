#! /usr/bin/env bash

python3 ./test_gen.py > tests.txt
python3 ./test.py < tests.txt > ./true_results.txt
./bigint < ./tests.txt > ./results.txt
diff ./results.txt ./true_results.txt
