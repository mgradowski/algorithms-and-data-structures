import enum
import random

RANDINT_BOUND = 1_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000_000
INITIAL_NUMBERS_COUNT = 20
CMD_COUNT = 300
READ_COUNT_MAX = 20 #100_000 // (2 * CMD_COUNT)


def get_random_number():
    return random.randint(-RANDINT_BOUND, RANDINT_BOUND)

class Commands(enum.Enum):
    SHOW_ALL = enum.auto()
    SHOW_MIN = enum.auto()
    SHOW_MAX = enum.auto()
    ADD = enum.auto()
    SUB = enum.auto()
    READ = enum.auto()

print(INITIAL_NUMBERS_COUNT)
for _ in range(INITIAL_NUMBERS_COUNT):
    print(get_random_number())

numbers_read = INITIAL_NUMBERS_COUNT

for _ in range(CMD_COUNT):
    cmd = random.choice(list(Commands))
    if cmd is Commands.SHOW_ALL:
        print('?')
    elif cmd is Commands.SHOW_MIN:
        print('min')
    elif cmd is Commands.SHOW_MAX:
        print('max')
    elif cmd is Commands.ADD:
        i = random.randint(0, numbers_read - 1)
        j = random.randint(0, numbers_read - 1)
        k = random.randint(0, numbers_read - 1)
        print(f"{i} = {j}  + {k}")
    elif cmd is Commands.SUB:
        i = random.randint(0, numbers_read - 1)
        j = random.randint(0, numbers_read - 1)
        k = random.randint(0, numbers_read - 1)
        print(f"{i} = {j}  - {k}")
    elif cmd is Commands.READ:
        n = random.randint(1, READ_COUNT_MAX)
        numbers_read += n
        print(n)
        for _ in range(n):
            print(get_random_number())

print('q')
