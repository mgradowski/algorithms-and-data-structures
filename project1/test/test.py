import re
import sys

QUIT = re.compile(r'^q$')
SHOW_ALL = re.compile(r'^\?$')
SHOW_MIN = re.compile(r'^min$')
SHOW_MAX = re.compile(r'^max$')
ADD = re.compile(r'^(0|\d+)\s*=\s*(0|\d+)\s*\+\s*(0|\d+)$')
SUB = re.compile(r'^(0|\d+)\s*=\s*(0|\d+)\s*\-\s*(0|\d+)$')
READ = re.compile(r'^([1-9][0-9]*)$')

nums = []

while cmd := sys.stdin.readline():
    if QUIT.match(cmd):
        break
    if SHOW_ALL.match(cmd):
        print('\n'.join(str(n) for n in nums))
    elif SHOW_MIN.match(cmd):
        print(min(nums))
    elif SHOW_MAX.match(cmd):
        print(max(nums))
    elif m := ADD.match(cmd):
        i, j, k = m.groups()
        nums[int(i)] = nums[int(j)] + nums[int(k)]
    elif m := SUB.match(cmd):
        i, j, k = m.groups()
        nums[int(i)] = nums[int(j)] - nums[int(k)]
    elif m := READ.match(cmd):
        (n,) = m.groups()
        for _ in range(int(n)) :
            nums.append(int(sys.stdin.readline()))
    elif cmd == '0\n':
        break
    else:
        print("error")
        break
        