#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>
#include <stdio.h>

#define SCNkey SCNdFAST32
#define PRIkey PRIdFAST32

#define PUSH 'i'
#define POP 'd'
#define GET_ITEM 'g'
#define EXIT 'q'


typedef int_fast32_t key_t;

typedef struct lifo_node_t
{
    key_t key;
    struct lifo_node_t* next;
} lifo_node_t;

typedef struct
{
    lifo_node_t* top;
    uint_fast32_t size;
} lifo_t;

lifo_t*
lifo_alloc(void)
{
    lifo_t* lifo = (lifo_t*)malloc(sizeof(lifo_t));
    lifo->top = NULL;
    lifo->size = 0;
    return lifo;
}

void
lifo_push(lifo_t* lifo, key_t key)
{
    lifo_node_t* node = (lifo_node_t*)malloc(sizeof(lifo_node_t));
    node->key = key;
    node->next = lifo->top;
    lifo->top = node;
    lifo->size += 1;
}

key_t
lifo_pop(lifo_t* lifo)
{
    key_t key = lifo->top->key;
    lifo_node_t* tmp_node = lifo->top;
    lifo->top = lifo->top->next;
    free(tmp_node);
    lifo->size -= 1;
    return key;
}

key_t
lifo_get_item(lifo_t* lifo, size_t i) {
    i = lifo->size - (i + 1);
    lifo_node_t* node = lifo->top;
    while(i--) {
        node = node->next;
    }
    return node->key;
}

void
lifo_free(lifo_t* lifo) {
    while (lifo->size > 0) {
        lifo_pop(lifo);
    }
    free(lifo);
}

int main(void) {
    char cmd_buf;
    size_t i_buf;
    key_t key_buf;

    lifo_t* lifo = lifo_alloc();

    while(( cmd_buf=getchar() ) != EXIT) {
        switch (cmd_buf)
        {
        case PUSH:
            scanf(" %" SCNkey, &key_buf);
            lifo_push(lifo, key_buf);
            break;
        case POP:
            if (lifo->size > 0) {
                printf("%" PRIkey "\n", lifo_pop(lifo));
            } else {
                puts("!");
            }
            break;
        case GET_ITEM:
            scanf(" %zu", &i_buf);
            if (lifo->size > i_buf) {
                printf("%" PRIkey "\n", lifo_get_item(lifo, i_buf));
            } else {
                puts("?");
            }
            break;
        default:
            fprintf(stderr, "Unexpected input '%c'.\n", cmd_buf);
            exit(EXIT_FAILURE);
            break;
        }
        // Skip whitespace so it's not read as a cmd.
        scanf(" ");
    }

    lifo_free(lifo);

    return EXIT_SUCCESS;
}